import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerFormComponent } from './beer-form.component';
import { describe } from 'jasmine';
import { beforeEach } from 'jasmine';
import { it } from 'jasmine';
import { expect } from 'jasmine';

describe('BeerFormComponent', () => {
  let component: BeerFormComponent;
  let fixture: ComponentFixture<BeerFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BeerFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BeerFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
