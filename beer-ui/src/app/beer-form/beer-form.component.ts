import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Beer} from '../model/beer';
import {BeerService} from '../service/beer.service';

@Component({
  selector: 'app-beer-form',
  templateUrl: './beer-form.component.html',
  styleUrls: ['./beer-form.component.css']
})
export class BeerFormComponent {

  beer: Beer;

  constructor(private route: ActivatedRoute,    private router: Router,    private beerService: BeerService  ) {
    this.beer = new Beer();
  }

  onSubmit() {
    this.beerService.save(this.beer).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/beers']);
  }

}
