import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BeerListComponent } from './beer-list/beer-list.component';
import { BeerFormComponent } from './beer-form/beer-form.component';
import { BeerService} from './service/beer.service';
import { UpdateBeerComponent } from './update-beer/update-beer.component';


@NgModule({
  declarations: [
    AppComponent,
    BeerListComponent,
    BeerFormComponent,
    UpdateBeerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [BeerService],
  bootstrap: [AppComponent]
})
export class AppModule { }
