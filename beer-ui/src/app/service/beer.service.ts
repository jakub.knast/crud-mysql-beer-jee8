
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Beer } from '../model/beer';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class BeerService {

  private beersUrl: string;

  constructor(private http: HttpClient) {
    const baseurl = window.location.protocol + '//' + window.location.hostname;
    this.beersUrl = baseurl + ':8080/api/v1/beers';
  }

  public findById(id: String) {
    return this.http.get<Beer>(this.beersUrl + '/' + id);
  }
  public findAll(): Observable<Beer[]> {
    return this.http.get<Beer[]>(this.beersUrl);
  }

  public save(beer: Beer) {
    return this.http.post<Beer>(this.beersUrl, beer);
  }

  public update(beer: Beer) {
    return this.http.put<Beer>(this.beersUrl + '/' + beer.id, beer);
  }

  public remove(beer: Beer) {
    return this.http.delete<Beer>(this.beersUrl + '/' + beer.id);
  }
}
