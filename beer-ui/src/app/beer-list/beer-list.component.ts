import {Component, OnInit} from '@angular/core';
import {BeerService} from '../service/beer.service';
import {Beer} from '../model/beer';

@Component({
  selector: 'app-beer-list',
  templateUrl: './beer-list.component.html',
  styleUrls: ['./beer-list.component.css']
})
export class BeerListComponent implements OnInit {

  beers: Beer[];

  constructor(private beerService: BeerService) {
  }

  ngOnInit() {
    this.beerService.findAll().subscribe(data => {
      this.beers = data;
    });
  }

  removeItem(beer: Beer) {
    this.beerService.remove(beer).subscribe(data => {
      // this.beers = data;
    //  console.log('yeees ' + data);
      this.beerService.findAll().subscribe(beers => {
        this.beers = beers;
      });
    });
    // .subscribe(data => {
    // this.beers = data;
    // });


  }
}
