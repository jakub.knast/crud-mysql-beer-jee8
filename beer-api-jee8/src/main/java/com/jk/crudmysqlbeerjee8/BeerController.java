package com.jk.crudmysqlbeerjee8;

import lombok.extern.slf4j.Slf4j;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;

//@RestController
//@RequestMapping("/api/v1/beers")
//@CrossOrigin
@Slf4j
@Path("/api/v1/beers")
public class BeerController {

    @Inject
    private BeerService beerService;

    @GET
    @Path("/")
    @Produces({"application/json"})
    public List<BeerEntity> findAll() {
        return beerService.findAll();
    }

    @POST
    @Produces({"application/json"})
    public Response create(
//            @Valid @RequestBody
            BeerEntity BeerEntity) {
        return  Response.status(201).entity(beerService.save(BeerEntity)).build();
    }

    @GET
    @Path("/{id}")
    @Produces({"application/json"})
    public Response findById(@PathParam("id") Long id) {
        Optional<BeerEntity> stock = beerService.findById(id);
        if (!stock.isPresent()) {
            log.error("Id " + id + " is not existed");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return Response.status(200).entity(stock.get()).build();
    }

    @PUT
    @Path("/{id}")
    @Produces({"application/json"})
    public Response update(@PathParam("id") Long id, BeerEntity beerEntity) {
        log.info("66666");
        log.info("11111");
        log.info("66666");
        log.info("66666");
        if (!beerService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        return  Response.status(200).entity(beerService.update(beerEntity)).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces({"application/json"})
    public Response delete(@PathParam("id") Long id) {
        if (!beerService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        beerService.deleteById(id);
        return  Response.status(200).build();
    }
}
