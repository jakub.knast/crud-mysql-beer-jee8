package com.jk.crudmysqlbeerjee8;

import lombok.Data;

//import org.hibernate.annotations.CreationTimestamp;
//import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Data
@Entity
@Table(name = "BeerEntity", uniqueConstraints = @UniqueConstraint(columnNames = "id"))
public class BeerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private BigDecimal percentage;
    private BigDecimal price;

//    @CreationTimestamp
    private Date createdAt;

//    @UpdateTimestamp
    private Date updatedAt;
}
