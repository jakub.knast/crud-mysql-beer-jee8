package com.jk.crudmysqlbeerjee8;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Order;

import javax.annotation.Resource;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.transaction.*;
import java.util.List;
import java.util.Optional;

@ApplicationScoped
@lombok.extern.slf4j.Slf4j
public class BeerRepository {

    @Resource
    private UserTransaction userTransaction;

    @Inject
    private EntityManager em;

    public Optional<BeerEntity> findById(Long id) {
        return Optional.ofNullable(em.find(BeerEntity.class, id));
    }

    @SuppressWarnings("unchecked")
    public List<BeerEntity> findAll() {

//        CriteriaBuilder cb = em.getCriteriaBuilder();
//        CriteriaQuery<BeerEntity> cq = cb.createQuery(BeerEntity.class);
//        cq.where(cb.equal(join.get("name"), "ordinary"));
//        TypedQuery<Accounts> tq = em.createQuery(cq);
//        List<Accounts> accounts = tq.getResultList();


//        em.createNamedQuery("asd", )
        Session session = (Session) em.getDelegate();
        Criteria cb = session.createCriteria(BeerEntity.class);
        cb.addOrder(Order.asc("name"));
        return (List<BeerEntity>) cb.list();
    }

    public BeerEntity save(BeerEntity beerEntity) {
        log.info("before persisting entity with the id: {}", beerEntity.getId());
        try {
            userTransaction.begin();
            em.persist(beerEntity);
            userTransaction.commit();
            log.info("after persisting entity with the id: {}", beerEntity.getId());
            return beerEntity;
        } catch (NotSupportedException | SystemException | HeuristicMixedException | HeuristicRollbackException | RollbackException e) {
            throw new RuntimeException(e);
        }
    }

    public BeerEntity update(BeerEntity beerEntity) {
        log.info("before merge entity with the id: {}", beerEntity.getId());
        try {
            userTransaction.begin();
            em.merge(beerEntity);
            userTransaction.commit();
            log.info("after merge entity with the id: {}", beerEntity.getId());
            return beerEntity;
        } catch (NotSupportedException | SystemException | HeuristicMixedException | HeuristicRollbackException | RollbackException e) {
            throw new RuntimeException(e);
        }
    }

    public void deleteById(Long id) {
        Optional<BeerEntity> byId = findById(id);
        byId.ifPresent((beer) -> {
            em.remove(beer);
        });
    }

}
