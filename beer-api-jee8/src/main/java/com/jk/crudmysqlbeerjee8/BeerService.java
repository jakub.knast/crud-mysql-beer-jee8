package com.jk.crudmysqlbeerjee8;

import javax.inject.Inject;
import java.util.List;
import java.util.Optional;


public class BeerService {
    @Inject
    private  BeerRepository beerRepository;

    public List<BeerEntity> findAll() {
        return beerRepository.findAll();
    }

    public Optional<BeerEntity> findById(Long id) {
        return beerRepository.findById(id);
    }

    public BeerEntity save(BeerEntity stock) {
        return beerRepository.save(stock);
    }
    public BeerEntity update(BeerEntity stock) {
        return beerRepository.update(stock);
    }

    public void deleteById(Long id) {
        beerRepository.deleteById(id);
    }
}
